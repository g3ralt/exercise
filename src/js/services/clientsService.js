import { requestOptions } from '.';

const getClientsFromAPI = () => {

    const options = requestOptions('GET');
    const resource = "https://api.myjson.com/bins/13pqgi";

    return fetch(resource, options)
        .then(res => {
            return res.status === 200 ? res.text() : Promise.reject('Couldn`t connect to API');
        })
        .then(text => JSON.parse(text));
};

export const clientsService = {
    getClientsFromAPI
};