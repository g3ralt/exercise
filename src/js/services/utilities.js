export const requestOptions = (method, data) => {
    let headers = {
        "Content-type": "Application/json"
    };
    
    let options = {
        method,
        headers
    };
    if (data !== undefined) {
        options.body = JSON.stringify(data);
    }
    return options;
};