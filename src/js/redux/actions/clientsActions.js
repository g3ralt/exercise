import { clientsConstants as C } from '../constants';
import { clientsService } from '../../services';

const getClients = () => {

    return dispatch => {

        dispatch(request());

        return clientsService.getClientsFromAPI()
            .then(arr => dispatch(success(arr)))
            .catch(err => dispatch(failure()));
    };

    function request() { return { type: C.REQUEST }; };
    function success(arr) { return { type: C.REQUEST_SUCCESS, payload: arr.clients }; };
    function failure() { return { type: C.REQUEST_ERROR }; };
};

export const clientsActions = {
    getClients
};