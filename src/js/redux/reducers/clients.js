import { clientsConstants as C } from '../constants';

export const clients = (state = { isFetching: false, collection: null }, action) => {
    switch (action.type) {
        case C.REQUEST:
            return {
                isFetching: true
            }
        case C.REQUEST_SUCCESS:
            return {
                isFetching: false,
                collection: action.payload
            };
        case C.REQUEST_ERROR:
            return {
                isFetching: false
            };
        default:
            return state;
    };
};