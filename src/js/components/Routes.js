import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ClientList, ClientView } from '../components/containers';

const Routes = () => {
    return (
        <Switch>
            <Route exact path="/" component={ClientList} />
            <Route exact path="/client/:id" component={ClientView} />
        </Switch>
    );
};

export default Routes;