import { connect } from 'react-redux';
import { App as AppUI } from '../ui/App';
import actions from '../../redux/actions';

const mapStateToProps = state =>
    ({

    });

const mapDispatchToProps = dispatch =>
    ({
        getAllClients() {
            return dispatch(actions.clientsActions.getClients());
        }
    });

export const App = connect(mapStateToProps, mapDispatchToProps)(AppUI);