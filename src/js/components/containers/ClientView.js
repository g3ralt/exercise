import { connect } from 'react-redux';
import { ClientView as ClientViewUI } from '../ui/ClientView';

const mapStateToProps = state =>
    ({
        clients: state.clients.collection
    });

const mapDispatchToProps = dispatch =>
    ({

    });

export const ClientView = connect(mapStateToProps, mapDispatchToProps)(ClientViewUI);