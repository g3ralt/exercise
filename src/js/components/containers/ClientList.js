import { connect } from 'react-redux';
import  ClientListUI  from '../ui/ClientList';

const mapStateToProps = state =>
    ({
        clients: state.clients
    });

const mapDispatchToProps = dispatch =>
    ({
        
    });

export const ClientList = connect(mapStateToProps, mapDispatchToProps)(ClientListUI);