import React from 'react';
import Container from 'react-bootstrap/Container';

const Header = () => {
    return (
        <header>
          <Container fluid className="py-3">
            <h1 className="d-block text-center text-uppercase h6 mb-0">
                logo
            </h1>
          </Container>
        </header>
    );
}

export default Header;