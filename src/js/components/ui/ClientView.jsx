import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { NearbyPlaces } from './NearbyPlaces';

export class ClientView extends React.Component {



    render() {
        const { match: { params: { id } }, clients } = this.props;

        const currentClient = clients ? clients.find(el => el.id == id) : {};
        const { address } = currentClient;


        return clients ? (
            <>
                <Row className="clientImg mb-5" style={{ backgroundImage: `url(${currentClient.image})` }} >

                </Row>
                <Container fluid>
                    <Row>
                        <Col sm={6} md={3} className="p-3">
                            <h2>Address</h2>
                            <h6>{`${address.number} ${address.street}`}</h6>
                            <h6>{`${address.city}, ${address.zip}`}</h6>
                        </Col>
                        <Col sm={6} md={3} className="p-3" >
                            <h2>Contact</h2>
                            <h6>{currentClient.phone}</h6>
                            <h6>{currentClient.email}</h6>
                        </Col>
                        <Col md={6} className="p-3 bg-white" >
                            <h2>Nearby places</h2>
                            <NearbyPlaces places={(() => {
                                return clients.filter(el => {
                                    return el.address.city === currentClient.address.city;
                                });
                            })()} />
                        </Col>
                    </Row>
                </Container>

            </>
        )
            : (
                <>
                    {/* Add a spinner */}
                </>
            );
    }
}