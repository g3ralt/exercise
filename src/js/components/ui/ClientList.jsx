import React from 'react';
import { Table } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';

class ClientListUI extends React.Component {

    

    createTableRows(arr) {
        const { history } = this.props;

        return arr.map(client => (
            <Table.Row className="bg-white" key={client.id} onClick={() => history.push(`/client/${client.id}`)}>

                <Table.Cell className="py-3 pl-4 ">{client.name}</Table.Cell>
                <Table.Cell className="py-3 pl-4 ">{client.description}</Table.Cell>
            </Table.Row>

        ));
    }

    render() {
        const { clients } = this.props;


        return (
            <section>
                <Table basic="very" singleLine selectable>
                    <Table.Header>
                        <Table.Row className="bg-white">
                            <Table.HeaderCell className="py-3 pl-4 text-uppercase color-th" width={1} >Name</Table.HeaderCell>
                            <Table.HeaderCell className="py-3 pl-4 text-uppercase color-th" width={3} >Description</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {clients.collection && this.createTableRows(clients.collection)}
                    </Table.Body>
                </Table>
            </section>
        );
    }
}


export default withRouter(ClientListUI);