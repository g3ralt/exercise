import React from 'react';
import Routes from '../Routes';
import Header from './Header';
import Container from 'react-bootstrap/Container';

export class App extends React.Component {

  componentDidMount() {
    this.props.getAllClients();
  }

  render() {

    return (
      <React.Fragment>
        <Header />

        <main className="bg-light p-5">
          <Container fluid>
            <Routes />
          </Container>


        </main>

        <footer>
        </footer>

      </React.Fragment>
    );
  }
}
