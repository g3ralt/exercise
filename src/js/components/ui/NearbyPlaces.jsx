import React from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

export const NearbyPlaces = ({ places = [] }) => {
    return (
        <section className="p-3" >
            {places.map((place, i) => {
                const { address: { street, number, zip, city }, name } = place;
                return (<Row key={i} className="bg-light mb-2 p-3" >
                    <Col xs={3}>
                        {name}
                    </Col>
                    <Col xs={9}>
                        {`${number} ${street}, ${city} ${zip}`}
                    </Col>
                </Row>)
            })}
        </section>
    );
};