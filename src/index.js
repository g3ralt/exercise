import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './js/components/containers';
import { Provider } from 'react-redux';
import { store as reduxStore } from './js/redux';
import { BrowserRouter as Router } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import './styles/main.css';

ReactDOM.render(
    <Provider store={reduxStore}>

        <Router>
            <App />
        </Router>

    </Provider>,
    document.getElementById('root')
);
